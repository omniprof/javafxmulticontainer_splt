package com.kenfogel.fishfxtree.view;

import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import com.kenfogel.fishfxtable.view.FishFXTableControllerDND;
import com.kenfogel.javafxmulticontainerv4_split.beans.FishData;
import com.kenfogel.javafxmulticontainerv4_split.persistence.FishDAO;
import javafx.scene.input.DragEvent;

/**
 * This controller began its life as part of a standalone display of a container
 * with a menu, tool bar and HTML editor. It is now part of another container.
 *
 * A method was added to allow the RootLayoutController to pass in a reference
 * to the FishFXTableController
 *
 * i18n added
 *
 * Added drag and drop
 *
 * @author Ken Fogel
 * @version 1.2
 *
 */
public class FishFXTreeControllerDND {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(FishFXTreeControllerDND.class);

    private FishDAO fishDAO;
    private FishFXTableControllerDND fishFXTableController;

    @FXML
    private TreeView<FishData> fishFXTreeView;

    // Resource bundle is injected when controller is loaded
    @FXML
    private ResourceBundle resources;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        // We need a root node for the tree and it must be the same type as all
        // nodes
        FishData rootFish = new FishData();

        // The tree will display common name so we set this for the root
        // Because we are using i18n the root name comes from the resource
        // bundle
        rootFish.setCommonName(resources.getString("Fishies"));

        fishFXTreeView.setRoot(new TreeItem<>(rootFish));

        // This cell factory is used to choose which field in the FishData object
        // is used for the node name
        fishFXTreeView.setCellFactory((e) -> new TreeCell<FishData>() {
            @Override
            protected void updateItem(FishData item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getCommonName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });

    }

    /**
     * When a drag is detected the control at the start of the drag is accessed
     * to determine what will be dragged.
     *
     * SceneBuilder writes the event as ActionEvent that you must change to the
     * proper event type that in this case is DragEvent
     *
     * @param event
     */
    @FXML
    private void dragDropped(DragEvent event) {
        LOG.debug("Tree Drag Drop");
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (event.getDragboard().hasString()) {
            String text = db.getString();
            LOG.debug("Table ID: " + text);
            success = true;
        }
        event.setDropCompleted(success);
        event.consume();
    }

    /**
     * Supports dragging from another component
     *
     * @param event
     */
    @FXML
    private void dragOver(DragEvent event) {
        /* data is dragged over the target */
        LOG.debug("onDragOver");

        // Accept it only if it is not dragged from the same control and if it
        // has a string data
        if (event.getGestureSource() != fishFXTreeView && event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * FishDAO object.
     *
     * @param fishDAO
     */
    public void setFishDAO(FishDAO fishDAO) {
        this.fishDAO = fishDAO;
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * FishFXTableController from which it can request a reference to the
     * TreeView. With theTreeView reference it can change the selection in the
     * TableView.
     *
     * @param fishFXTableController
     */
    public void setTableController(FishFXTableControllerDND fishFXTableController) {
        this.fishFXTableController = fishFXTableController;
    }

    /**
     * Build the tree from the database
     *
     * @throws SQLException
     */
    public void displayTree() throws SQLException {
        // Retrieve the list of fish
        ObservableList<FishData> fishies = fishDAO.findTableAll();

        // Build an item for each fish and add it to the root
        if (fishies != null) {
            fishies.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
                item.setGraphic(new ImageView(getClass().getResource("/images/fish.png").toExternalForm()));
                return item;
            }).forEachOrdered((item) -> {
                fishFXTreeView.getRoot().getChildren().add(item);
            });
        }

        // Open the tree
        fishFXTreeView.getRoot().setExpanded(true);

        // Listen for selection changes and show the fishData details when
        // changed.
        fishFXTreeView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showFishDetailsTree(newValue));
    }

    /**
     * Using the reference to the FishFXTableControllerDND it can change the
 selected row in the TableView It also displays the FishData object that
 corresponds to the selected node.
     *
     * @param fishData
     */
    private void showFishDetailsTree(TreeItem<FishData> fishData) {

        // Select the row that contains the FishData object from the Tree
        fishFXTableController.getfishDataTable().getSelectionModel().select(fishData.getValue());
        // Get the row number
        int x = fishFXTableController.getfishDataTable().getSelectionModel().getSelectedIndex();
        // Scroll the table so that the row is at the top of the displayed table
        fishFXTableController.getfishDataTable().scrollTo(x);

        LOG.info("showFishDetailsTree\n" + fishData.getValue());
    }

}
