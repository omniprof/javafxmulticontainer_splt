package com.kenfogel.fishfxtable.view;

import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

import com.kenfogel.javafxmulticontainerv4_split.beans.FishData;
import com.kenfogel.javafxmulticontainerv4_split.persistence.FishDAO;
import javafx.event.EventHandler;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller began its life as part of a standalone display of a container
 * with a menu, tool bar and HTML editor. It is now part of another container.
 *
 * A method was added to allow access to the TreeView object.
 *
 * I18N is added but the HTMLEditor will only appear in a target language if the
 * program runs on an OS in that language.
 *
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class FishFXTableControllerDND {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(FishFXTableControllerDND.class);

    private FishDAO fishDAO;

    // Resource bundle is injected when controller is loaded
    @FXML
    private ResourceBundle resources;

    @FXML
    private AnchorPane fishFXTable;

    @FXML
    private TableView<FishData> fishDataTable;

    @FXML
    private TableColumn<FishData, Number> idColumn;

    @FXML
    private TableColumn<FishData, String> commonNameColumn;

    @FXML
    private TableColumn<FishData, String> latinColumn;

    @FXML
    private TableColumn<FishData, String> phColumn;

    @FXML
    private TableColumn<FishData, String> khColumn;

    @FXML
    private TableColumn<FishData, String> tempColumn;

    @FXML
    private TableColumn<FishData, String> fishSizeColumn;

    @FXML
    private TableColumn<FishData, String> speciesOriginColumn;

    @FXML
    private TableColumn<FishData, String> tankSizeColumn;

    @FXML
    private TableColumn<FishData, String> stockingColumn;

    @FXML
    private TableColumn<FishData, String> dietColumn;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    public void initialize() {

        // Connects the property in the FishData object to the column in the
        // table
        idColumn.setCellValueFactory(cellData -> cellData.getValue()
                .idProperty());
        commonNameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .commonNameProperty());
        latinColumn.setCellValueFactory(cellData -> cellData.getValue()
                .latinProperty());
        phColumn.setCellValueFactory(cellData -> cellData.getValue()
                .phProperty());
        khColumn.setCellValueFactory(cellData -> cellData.getValue()
                .khProperty());
        tempColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tempProperty());
        fishSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .fishSizeProperty());
        speciesOriginColumn.setCellValueFactory(cellData -> cellData.getValue()
                .speciesOriginProperty());
        tankSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tankSizeProperty());
        stockingColumn.setCellValueFactory(cellData -> cellData.getValue()
                .stockingProperty());
        dietColumn.setCellValueFactory(cellData -> cellData.getValue()
                .dietProperty());

        adjustColumnWidths();

        // Listen for selection changes and show the fishData details when
        // changed.
        fishDataTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showFishDetails(newValue));
    }

    /**
     * When you select and drag a row this method is called and places the ID in
     * the Clipboard
     *
     * @param event
     */
    @FXML
    private void dragDetected(MouseEvent event) {
        String selected = "" + fishDataTable.getSelectionModel().getSelectedItem().getId();
        LOG.debug("Selected row ID: " + selected);
        if (selected != null) {
            Dragboard db = fishDataTable.startDragAndDrop(TransferMode.ANY);
            ClipboardContent content = new ClipboardContent();
            content.putString(selected);
            db.setContent(content);
            event.consume();
        }

    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * FishDAO object.
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDAO(FishDAO fishDAO) throws SQLException {
        this.fishDAO = fishDAO;
    }

    /**
     * The table displays the fish data
     *
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        fishDataTable.setItems(this.fishDAO.findTableAll());
    }

    /**
     * The FishFXTreeController needs a reference to the this controller. With
     * that reference it can call this method to retrieve a reference to the
     * TableView and change its selection
     *
     * @return
     */
    public TableView<FishData> getfishDataTable() {
        return fishDataTable;
    }

    /**
     * Sets the width of the columns based on a percentage of the overall width
     *
     * This needs to enhanced so that it uses the width of the anchor pane it is
     * in and then changes the width as the table grows.
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = fishFXTable.getPrefWidth();
        // Set width of each column
        idColumn.setPrefWidth(width * .05);
        commonNameColumn.setPrefWidth(width * .15);
        latinColumn.setPrefWidth(width * .15);
        phColumn.setPrefWidth(width * .05);
        khColumn.setPrefWidth(width * .05);
        tempColumn.setPrefWidth(width * .05);
        fishSizeColumn.setPrefWidth(width * .1);
        speciesOriginColumn.setPrefWidth(width * .1);
        tankSizeColumn.setPrefWidth(width * .1);
        stockingColumn.setPrefWidth(width * .1);
        dietColumn.setPrefWidth(width * .1);
    }

    /**
     * To be able to test the selection handler for the table, this method
     * displays the FishData object that corresponds to the selected row.
     *
     * @param fishData
     */
    private void showFishDetails(FishData fishData) {
        System.out.println("showFishDetails\n" + fishData);
    }

}
