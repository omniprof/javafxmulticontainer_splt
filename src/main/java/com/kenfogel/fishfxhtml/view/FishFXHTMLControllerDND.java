package com.kenfogel.fishfxhtml.view;

import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.javafxmulticontainerv4_split.beans.FishData;
import com.kenfogel.javafxmulticontainerv4_split.persistence.FishDAO;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.web.HTMLEditor;

/**
 * This controller began its life as part of a standalone display of a container
 * with a menu, tool bar and HTML editor. It is now part of another container.
 * Nothing was changed when added to the new program.
 *
 * i18n added
 *
 * Added drag and drop
 *
 * @author Ken Fogel
 * @version 1.2
 *
 */
public class FishFXHTMLControllerDND {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(FishFXHTMLControllerDND.class);

    // We will need this to read from the database
    private FishDAO fishDAO;

    @FXML
    private HTMLEditor fishFXHTMLEditor;

    // Resource bundle is injected when controller is loaded
    @FXML
    private ResourceBundle resources;

    /**
     * This method prevents dropping the value on anything but the
     * FXHTMLEditor,
     *
     * SceneBuilder writes the event as ActionEvent that you must change to the
     * proper event type that in this case is DragEvent
     *
     * @param event
     */
    @FXML
    private void dragOver(DragEvent event) {
        /* data is dragged over the target */
        LOG.debug("onDragOver");

        // Accept it only if it is not dragged from the same control and if it
        // has a string data
        if (event.getGestureSource() != fishFXHTMLEditor && event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    /**
     * When the mouse is released over the FXHTMLEditor the value is written to
     * the editor.
     *
     * SceneBuilder writes the event as ActionEvent that you must change to the
     * proper event type that in this case is DragEvent
     *
     * @param event
     */
    @FXML
    private void dragDropped(DragEvent event) {
        LOG.debug("onDragDropped");
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            fishFXHTMLEditor.setHtmlText(db.getString());
            success = true;
        }
        //let the source know whether the string was successfully transferred
        // and used
        event.setDropCompleted(success);

        event.consume();
    }

    /**
     * This just displays the contents of the HTMLEditor
     */
    @FXML
    private void handleSave() {
        System.out.println(fishFXHTMLEditor.getHtmlText());
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        // Modal dialog box
        Alert dialog = new Alert(AlertType.INFORMATION);
        dialog.setTitle(resources.getString("HTMLDemo"));
        dialog.setHeaderText(resources.getString("About"));
        dialog.setContentText(resources.getString("Demo"));
        dialog.show();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleExit() {
        Platform.exit();
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * FishDAO object.
     *
     * @param fishDAO
     */
    public void setFishDAO(FishDAO fishDAO) {
        this.fishDAO = fishDAO;
    }

    /**
     * Convert the first three fields from the first three records into HTML.
     */
    public void displayFishAsHTML() {
        ObservableList<FishData> data = null;
        try {
            data = fishDAO.findTableAll();
        } catch (SQLException ex) {
            LOG.error(null, ex);
            errorAlert("displayFishAsHTML()");
            Platform.exit();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<html><body contenteditable='false'>");
        if (data == null || data.isEmpty()) {
            sb.append("No data found");
        } else {
            for (int x = 0; x < 3; ++x) {
                sb.append(data.get(x).getId()).append("</br>");
                sb.append(data.get(x).getCommonName()).append("</br>");
                sb.append(data.get(x).getLatin()).append("</br></br>");
            }
        }
        sb.append("</body></html>");

        fishFXHTMLEditor.setHtmlText(sb.toString());

    }

    /**
     * Displays html that contains an embedded image
     */
    public void displayOtherHTML() {
        String other = "<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in this email.</h1><img src=\""
                + getClass().getResource("/FreeFall.jpg") + "\"><h2>I'm flying!</h2></body></html>";

        fishFXHTMLEditor.setHtmlText(other);
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }

}
