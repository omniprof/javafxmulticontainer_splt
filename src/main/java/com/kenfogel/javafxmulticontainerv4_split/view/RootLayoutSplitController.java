package com.kenfogel.javafxmulticontainerv4_split.view;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.kenfogel.fishfxhtml.view.FishFXHTMLControllerDND;
import com.kenfogel.fishfxtable.view.FishFXTableControllerDND;
import com.kenfogel.fishfxtree.view.FishFXTreeControllerDND;
import com.kenfogel.javafxmulticontainerv4_split.MainAppFX;
import com.kenfogel.javafxmulticontainerv4_split.persistence.FishDAO;
import com.kenfogel.javafxmulticontainerv4_split.persistence.FishDAOImpl;
import javafx.application.Platform;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the root layout. All of the other layouts are added in code here.
 * This allows us to use the standalone containers with minimal changes.
 *
 * i18n added
 *
 * Added proper logging
 *
 * @author Ken Fogel
 * @version 1.2
 *
 */
public class RootLayoutSplitController {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(RootLayoutSplitController.class);

    @FXML
    private AnchorPane upperLeftSplit;

    @FXML
    private AnchorPane upperRightSplit;

    @FXML
    private AnchorPane lowerLeftSplit;

    @FXML
    private AnchorPane lowerRightSplit;

    @FXML
    private ResourceBundle resources;

    private final FishDAO fishDAO;
    private FishFXTreeControllerDND fishFXTreeController;
    private FishFXTableControllerDND fishFXTableController;
    //private FishFXWebViewController fishFXWebViewController;
    private FishFXHTMLControllerDND fishFXHTMLController;

    public RootLayoutSplitController() {
        fishDAO = new FishDAOImpl();
    }

    /**
     * Here we call upon the methods that load the other containers and then
     * send the appropriate action command to each container
     */
    @FXML
    private void initialize() {

        initUpperLeftLayout();
        initUpperRightLayout();
        initLowerLeftLayout();
        initLowerRightLayout();

        // Tell the tree about the table
        setTableControllerToTree();

        try {
            fishFXTreeController.displayTree();
            fishFXTableController.displayTheTable();
            //fishFXHTMLController.displayFishAsHTML();
        } catch (SQLException ex) {
            LOG.error("initialize error", ex);
            errorAlert("initialize()");
            Platform.exit();
        }
    }

    /**
     * Send the reference to the FishFXTableControllerDND to the
 FishFXTreeController
     */
    private void setTableControllerToTree() {
        fishFXTreeController.setTableController(fishFXTableController);
    }

    /**
     * The TreeView Layout
     */
    private void initUpperLeftLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class
                    .getResource("/fxml/FishFXTreeLayout.fxml"));
            AnchorPane treeView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTreeController = loader.getController();
            fishFXTreeController.setFishDAO(fishDAO);

            upperLeftSplit.getChildren().add(treeView);
        } catch (IOException ex) {
            LOG.error("initUpperLeftLayout error", ex);
            errorAlert("initUpperLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The TableView Layout
     */
    private void initUpperRightLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class
                    .getResource("/fxml/FishFXTableLayout.fxml"));
            AnchorPane tableView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXTableController = loader.getController();
            fishFXTableController.setFishDAO(fishDAO);

            upperRightSplit.getChildren().add(tableView);
        } catch (SQLException | IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            errorAlert("initUpperRightLayout()");
            Platform.exit();
        }
    }

    /**
     * The WebView Layout
     */
    private void initLowerLeftLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class
                    .getResource("/fxml/FishFXWebViewLayout.fxml"));
            AnchorPane webView = (AnchorPane) loader.load();

            // Retrieve the controller if you must send it messages
            //fishFXWebViewController = loader.getController();
            lowerLeftSplit.getChildren().add(webView);
        } catch (IOException ex) {
            LOG.error("initLowerLeftLayout error", ex);
            errorAlert("initLowerLeftLayout()");
            Platform.exit();
        }
    }

    /**
     * The HTMLEditor Layout
     */
    private void initLowerRightLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(RootLayoutSplitController.class
                    .getResource("/fxml/FishFXHTMLLayout.fxml"));
            AnchorPane htmlView = (AnchorPane) loader.load();

            // Give the controller the data object.
            fishFXHTMLController = loader.getController();
            fishFXHTMLController.setFishDAO(fishDAO);

            lowerRightSplit.getChildren().add(htmlView);
        } catch (IOException ex) {
            LOG.error("initLowerRightLayout error", ex);
            errorAlert("initLowerRightLayout()");
            Platform.exit();
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }

}
