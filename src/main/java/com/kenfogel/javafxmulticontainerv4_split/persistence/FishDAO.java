package com.kenfogel.javafxmulticontainerv4_split.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.kenfogel.javafxmulticontainerv4_split.beans.FishData;

import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author Ken Fogel
 */
public interface FishDAO {

    // Create
    public int create(FishData fishData) throws SQLException;

    // Read
    public ObservableList<FishData> findTableAll() throws SQLException;

    public FishData findID(int id) throws SQLException;

    // Update
    public int update(FishData fishData) throws SQLException;

    // Delete
    public int delete(int ID) throws SQLException;
}
